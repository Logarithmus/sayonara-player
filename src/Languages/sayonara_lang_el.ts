<?xml version="1.0" ?><!DOCTYPE TS><TS language="el" sourcelanguage="en" version="2.1">
<context>
    <name>GUI_AlternativeCovers</name>
    <message>
        <location filename="../src/Gui/Covers/GUI_AlternativeCovers.ui" line="+14"/>
        <location line="+279"/>
        <source>Search cover</source>
        <translation>Αναζήτηση εξώφυλλου</translation>
    </message>
    <message>
        <location line="-263"/>
        <source>Online Search</source>
        <translation>Αναζήτηση στο διαδίκτυο</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Start search automatically</source>
        <translation>Αυτόματη έναρξη αναζήτησης</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Text or url</source>
        <translation>κείμενο ή url</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Search for</source>
        <translation>αναζήτηση για</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Server</source>
        <translation>Διακομιστής</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Automatic search</source>
        <translation>Αυτόματη αναζήτηση</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Local Search</source>
        <translation>Τοπική αναζήτηση</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Find covers in directory</source>
        <translation>Εύρεση εξώφυλλων σε φάκελο</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/Gui/Covers/GUI_AlternativeCovers.cpp" line="+299"/>
        <source>%n cover(s) found</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+143"/>
        <location line="+27"/>
        <source>Also save cover to %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-11"/>
        <source>Cover web search is not enabled</source>
        <translation>Η αναζήτηση εξώφυλλων στο διαδίκτυο δεν είναι ενεργοποιημένη</translation>
    </message>
</context>
<context>
    <name>GUI_History</name>
    <message>
        <location filename="../src/Gui/History/GUI_History.ui" line="+14"/>
        <source>Dialog</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/History/GUI_History.cpp" line="+50"/>
        <source>Load more entries</source>
        <translation>Φόρτωση περισσότερων καταχωρήσεων</translation>
    </message>
    <message>
        <location line="+139"/>
        <source>Scroll to top</source>
        <translation>Μετακινηθείτε προς τα πάνω</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll to bottom</source>
        <translation>Μετακινηθείτε προς τα κάτω</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select date range</source>
        <translation>Επιλέξτε εύρος ημερομηνίας</translation>
    </message>
</context>
<context>
    <name>InfoDialog</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_InfoDialog.ui" line="+20"/>
        <source>Info / Edit</source>
        <translation>Πληροφορίες / Επεξεργασία</translation>
    </message>
    <message>
        <location line="+274"/>
        <source>Loading files...</source>
        <translation>Φόρτωση αρχείων...</translation>
    </message>
</context>
<context>
    <name>GUI_Lyrics</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_Lyrics.ui" line="+38"/>
        <source>switch</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+56"/>
        <source>Zoom</source>
        <translation>Μεγέθυση</translation>
    </message>
    <message>
        <location line="+85"/>
        <source>Save Lyrics</source>
        <translation>Αποθήκευση στίχων</translation>
    </message>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_Lyrics.cpp" line="+314"/>
        <source>Save lyrics not supported</source>
        <translation>Δεν υποστηρίζεται η αποθήκευση στίχων</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Overwrite lyrics</source>
        <translation>Αντικατάσταση στίχων</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save lyrics</source>
        <translation>Αποθήκευση στίχων</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Source</source>
        <translation>Πηγή</translation>
    </message>
</context>
<context>
    <name>GUI_ImportDialog</name>
    <message>
        <location filename="../src/Gui/Library/GUI_ImportDialog.ui" line="+14"/>
        <source>Import</source>
        <translation>Εισαγωγή</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Import tracks to library</source>
        <translation>Εισαγωγή κομματιών στη βιβλιοθήκη</translation>
    </message>
    <message>
        <location line="+98"/>
        <source>Select target folder</source>
        <translation>Διαλέξτε φάκελο προορισμού</translation>
    </message>
    <message>
        <location filename="../src/Gui/Library/GUI_ImportDialog.cpp" line="+123"/>
        <source>Loading tracks</source>
        <translation>Φόρτωση κομματιών</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>No tracks</source>
        <translation>Κανένα κομμάτι</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Importing</source>
        <translation>Εισαγωγή</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Finished</source>
        <translation>Ολοκληρώθηκε</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Rollback</source>
        <translation>Επαναφορά</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cancelled</source>
        <translation>Ακυρώθηκε</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Choose target directory</source>
        <translation>Διαλέξτε φάκελο προορισμού</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>%1&lt;br /&gt;is no library directory</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_LocalLibrary</name>
    <message>
        <location filename="../src/Gui/Library/GUI_LocalLibrary.ui" line="+17"/>
        <source>Library</source>
        <translation>Βιβλιοθήκη</translation>
    </message>
    <message>
        <location line="+469"/>
        <source>Directory does not exist</source>
        <translation>Ο φάκελος δεν υπάρχει</translation>
    </message>
    <message>
        <location filename="../src/Gui/Library/GUI_LocalLibrary.cpp" line="+376"/>
        <source>Audio files</source>
        <translation>Αρχεία ήχου</translation>
    </message>
</context>
<context>
    <name>GUI_Logger</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Logger.ui" line="+29"/>
        <source>Module</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Player/GUI_Logger.cpp" line="+279"/>
        <source>Cannot open file</source>
        <translation>Αδύνατο το άνοιγμα αρχείου</translation>
    </message>
</context>
<context>
    <name>GUI_Player</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Player.ui" line="+14"/>
        <source>Sayonara Player</source>
        <translation>Sayonara Player</translation>
    </message>
</context>
<context>
    <name>GUI_AudioConverter</name>
    <message>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.ui" line="+45"/>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.cpp" line="+273"/>
        <source>Start</source>
        <translation>Έναρξη</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Stop</source>
        <translation>Στοπ</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>#Threads</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+39"/>
        <location line="+187"/>
        <source>Quality</source>
        <translation>Ποιότητα</translation>
    </message>
    <message>
        <location line="-164"/>
        <location line="+52"/>
        <location line="+89"/>
        <source>Bitrate</source>
        <translation>Bitrate</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.cpp" line="-162"/>
        <source>Audio Converter</source>
        <translation>Μετατροπή ήχου</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Threads</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+27"/>
        <location line="+121"/>
        <source>Cannot find encoder</source>
        <translation>Δε βρέθηκε κωδικοποιητής</translation>
    </message>
    <message>
        <location line="-109"/>
        <location line="+13"/>
        <source>Playlist does not contain tracks which are supported by the converter</source>
        <translation>Η λίστα τραγουδιών δεν περιέχει κομμάτια που υποστηρίζονται για μετατροπή</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>No track will be converted.</source>
        <translation>Κανένα κομμάτι δε θα μετατραπεί</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>These tracks will be ignored</source>
        <translation>Αυτά τα κομμάτια θα παραλειφθούν</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Choose target directory</source>
        <translation>Διαλέξτε φάκελο προορισμού</translation>
    </message>
    <message numerus="yes">
        <location line="+30"/>
        <source>Failed to convert %n track(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Please check the log files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>All tracks could be converted</source>
        <translation>Όλα τα κομμάτια μπόρεσαν να μετατραπούν</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Successfully finished</source>
        <translation>Ολοκληρώθηκε με επιτυχία</translation>
    </message>
</context>
<context>
    <name>GUI_Bookmarks</name>
    <message>
        <location filename="../src/Gui/Plugins/Bookmarks/GUI_Bookmarks.ui" line="+116"/>
        <source>Loop</source>
        <translation>Συνεχή επανάληψη</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Bookmarks/GUI_Bookmarks.cpp" line="+77"/>
        <location line="+53"/>
        <source>No bookmarks found</source>
        <translation>Δε βρέθηκαν σελιδοδείκτες</translation>
    </message>
    <message>
        <location line="+122"/>
        <source>Sorry, bookmarks can only be set for library tracks at the moment.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_Broadcast</name>
    <message>
        <location filename="../src/Gui/Plugins/Broadcasting/GUI_Broadcast.ui" line="+88"/>
        <source>Cannot Broadcast</source>
        <translation>Η εκπομπή είναι αδύνατη</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Broadcasting/GUI_Broadcast.cpp" line="+104"/>
        <source>Dismiss</source>
        <translation>Απόρριψη</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dismiss all</source>
        <translation>Απόρριψη όλων</translation>
    </message>
    <message numerus="yes">
        <location line="+51"/>
        <source>%n listener(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Cannot broadcast on port %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Maybe another application is using this port?</source>
        <translation>Ίσως μια άλλη εφαρμογή χρησιμοποιεί την συγκεκριμένη θύρα</translation>
    </message>
</context>
<context>
    <name>GUI_Speed</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.ui" line="+48"/>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+134"/>
        <source>Speed</source>
        <translation>Ταχύτητα</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Preserve pitch</source>
        <translation>Διατήρηση τόνου</translation>
    </message>
    <message>
        <location line="+30"/>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+1"/>
        <source>Pitch</source>
        <translation>Τόνος</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+2"/>
        <source>%1 and %2</source>
        <translation>%1 και %2</translation>
    </message>
    <message>
        <location line="+70"/>
        <location line="+1"/>
        <source>%1 not found</source>
        <translation>%1 δεν βρέθηκε</translation>
    </message>
</context>
<context>
    <name>GUI_Style</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Style.ui" line="+14"/>
        <source>Style</source>
        <translation>Στυλ</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Spectrum</source>
        <translation>Φάσμα</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Vert. spacing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <location line="+88"/>
        <source>Rect height</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-68"/>
        <location line="+44"/>
        <source>Hor. spacing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-30"/>
        <location line="+75"/>
        <source>Fading steps</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-51"/>
        <source>Level</source>
        <translation>Ένταση</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Rect width</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Ver. spacing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+54"/>
        <source>Style settings</source>
        <translation>Ρυθμίσεις εμφάνισης</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Color 2</source>
        <translation>Χρώμα 2</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Color 1</source>
        <translation>Χρώμα 1</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Color 3</source>
        <translation>Χρώμα 3</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Color 4</source>
        <translation>Χρώμα 4</translation>
    </message>
</context>
<context>
    <name>GUI_TargetPlaylistDialog</name>
    <message>
        <location filename="../src/Gui/Plugins/PlaylistChooser/GUI_TargetPlaylistDialog.ui" line="+14"/>
        <source>Choose target playlist</source>
        <translation>Διαλέξτε λίστα τραγουδιών προορισμού</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>relative filepaths</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+34"/>
        <source>Close</source>
        <translation>Κλείσιμο</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save</source>
        <translation>Αποθήκευση</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save playlist as...</source>
        <translation>Αποθήκευση λίστας τραγουδιών ως...</translation>
    </message>
</context>
<context>
    <name>GUI_StationSearcher</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_StationSearcher.ui" line="+14"/>
        <source>Search Radio Station</source>
        <translation>Αναζήτηση ραδιοφωνικού σταθμού</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_StationSearcher.cpp" line="+59"/>
        <source>Show radio stations from %1 to %2</source>
        <translation>Εμφάνιση ραδιοφωνικών σταθμών από 1% εώς 2%</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>Country</source>
        <translation>Χώρα</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Type</source>
        <translation>Τύπος</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Url</source>
        <translation>Url</translation>
    </message>
</context>
<context>
    <name>GUI_BroadcastPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Broadcast/GUI_BroadcastPreferences.ui" line="+20"/>
        <source>Port</source>
        <translation>Θύρα</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Ask for permission</source>
        <translation>Ερώτηση για άδεια</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Broadcast/GUI_BroadcastPreferences.cpp" line="+161"/>
        <source>Port %1 already in use</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_CoverPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Covers/GUI_CoverPreferences.ui" line="+62"/>
        <source>Inactive</source>
        <translation>Ανενεργό</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Active</source>
        <translation>Ενεργό</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Delete all covers from the database</source>
        <translation>Διαγραφή όλων των εξώφυλλων από τη βάση δεδομένων</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear cache</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Delete all covers from the Sayonara directory</source>
        <translation>Διαγραφή όλων των εξωφύλλων από τον φάκελο Sayonara</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete files</source>
        <translation>Διαγραφή αρχείων</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Save found covers into the library directory where the audio files are located</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Save found covers into library</source>
        <translation>Αποθήκευση των εξωφύλλων που βρέθηκαν στη βιβλιοθήκη</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Fetch missing covers from the internet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Saving covers to the database leads to significantly faster access but results in a bigger database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Save found covers to database</source>
        <translation>Αποθήκευση των εξωφύλλων που βρέθηκαν στη βάση δεδομένων</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Save found covers into Sayonara directory</source>
        <translation>Αποθήκευση των αποθηκευμένων εξωφύλλων στο φάκελο Sayonara</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Cover name</source>
        <translation>Όνομα εξώφυλλου</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Name of cover file</source>
        <translation>Όνομα αρχείου εξώφυλλου</translation>
    </message>
</context>
<context>
    <name>GUI_LanguagePreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Language/GUI_LanguagePreferences.ui" line="+24"/>
        <source>English</source>
        <translation>Αγγλικά</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Check for update</source>
        <translation>Έλεγχος για ενημέρωση</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Import new language</source>
        <translation>Εισαγωγή νέας γλώσσας</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>For new languages I am always looking for translators</source>
        <translation>Ζητούνται πάντα μεταφραστές για επιπλέον γλώσσες</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Language/GUI_LanguagePreferences.cpp" line="+62"/>
        <source>Language</source>
        <translation>Γλώσσα</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Cannot check for language update</source>
        <translation>Ο έλεγχος για ενημερώσεις γλώσσας είναι αδύνατος</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Language is up to date</source>
        <translation>Η γλώσσα έχει ενημερωθεί</translation>
    </message>
    <message>
        <location line="+28"/>
        <location line="+24"/>
        <source>Cannot fetch language update</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-10"/>
        <source>Language was updated successfully</source>
        <translation>Η γλώσσα ενημερώθηκε επιτυχώς</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>The language file could not be imported</source>
        <translation>Δεν μπόρεσε να γίνει εισαγωγή του αρχείου γλώσσας</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The language file was imported successfully</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_LastFmPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/LastFM/GUI_LastFmPreferences.ui" line="+37"/>
        <source>Password</source>
        <translation>Κωδικός</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Login now</source>
        <translation>Σύνδεση τώρα</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Scrobble time</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Username</source>
        <translation>Όνομα χρήστη</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/LastFM/GUI_LastFmPreferences.cpp" line="+162"/>
        <source>Logged in</source>
        <translation>Συνδεδεμένος</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Not logged in</source>
        <translation>Δεν συνδεθήκατε</translation>
    </message>
</context>
<context>
    <name>GUI_LibraryPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Library/GUI_LibraryPreferences.ui" line="+27"/>
        <source>Libraries</source>
        <translation>Βιβλιοθήκες</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Library-Playlist Interaction</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>When drag and drop into playlist </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <location line="+36"/>
        <source>do nothing (default)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-29"/>
        <source>start if stopped and playlist is empty</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>On double click, create playlist and</source>
        <translation>Με διπλό κλικ, δημιουργία λίστας τραγουδιών και</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>start playback if stopped</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>start playback immediately</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>(this is ignored when playlist is in &apos;append mode&apos;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>Other</source>
        <translation>Άλλα</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show &quot;Clear selection&quot; buttons</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Ignore English article &quot;The&quot; in artist name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Library/GUI_LibraryPreferences.cpp" line="+147"/>
        <source>Cannot edit library</source>
        <translation>Αδύνατη η επεξεργασία βιβλιοθήκης</translation>
    </message>
</context>
<context>
    <name>GUI_NotificationPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Notifications/GUI_NotificationPreferences.ui" line="+17"/>
        <source>Timeout (ms)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Notifications/GUI_NotificationPreferences.cpp" line="+98"/>
        <source>Notifications</source>
        <translation>Ειδοποιήσεις</translation>
    </message>
</context>
<context>
    <name>GUI_PlayerPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Player/GUI_PlayerPreferences.ui" line="+17"/>
        <source>Hide instead of close</source>
        <translation>Απόκρυψη αντί για κλείσιμο</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Start hidden</source>
        <translation>Έναρξη σε απόκρυψη</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>Update notifications</source>
        <translation>Ειδοποιήσεις ενημερώσεων</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show system tray icon</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Player/GUI_PlayerPreferences.cpp" line="+108"/>
        <source>This might cause Sayonara not to show up again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>In this case use the &apos;--show&apos; option at the next startup.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_PlaylistPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Playlist/GUI_PlaylistPreferences.ui" line="+36"/>
        <source>Behavior</source>
        <translation>Συμπεριφορά</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Start up</source>
        <translation>Εκκίνηση</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Load temporary playlists</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Load saved playlists</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+17"/>
        <source>Start playing</source>
        <translation>Έναρξη αναπαραγωγής</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Load last track on startup</source>
        <translation>Φόρτωση τελευταίου κομματιού κατά την έναρξη</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remember time of last track</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Stop behaviour</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Load last track after pressing stop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Look</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Show covers</source>
        <translation>Προβολή εξωφύλλων</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show numbers</source>
        <translation>Εμφάνιση αριθμών</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Show rating</source>
        <translation>Εμφάνιση αξιολόγησης</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show clear button</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>&apos;italic text&apos;</source>
        <translation>‘πλάγια γραφή’</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Example</source>
        <translation>Παράδειγμα</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>*bold text*</source>
        <translation>*έντονη γραφή*</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Show footer</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Playlist/GUI_PlaylistPreferences.cpp" line="+216"/>
        <source>Playlist look: Invalid expression</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_PreferenceDialog</name>
    <message>
        <location filename="../src/Gui/Preferences/PreferenceDialog/GUI_PreferenceDialog.ui" line="+150"/>
        <source>Preferences</source>
        <translation>Προτιμήσεις</translation>
    </message>
</context>
<context>
    <name>GUI_ProxyPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Proxy/GUI_ProxyPreferences.ui" line="+33"/>
        <source>Port</source>
        <translation>Θύρα</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Save username/password</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Host</source>
        <translation>Υπολογιστής</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Active</source>
        <translation>Ενεργό</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Automatic search</source>
        <translation>Αυτόματη αναζήτηση</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Password</source>
        <translation>Κωδικός</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Username</source>
        <translation>Όνομα χρήστη</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Proxy/GUI_ProxyPreferences.cpp" line="+57"/>
        <source>Proxy</source>
        <translation>Διαμεσολαβητής</translation>
    </message>
</context>
<context>
    <name>GUI_RemoteControlPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/RemoteControl/GUI_RemoteControlPreferences.ui" line="+36"/>
        <source>Detectable via UDP</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+31"/>
        <source>Remote control URL</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Port</source>
        <translation>Θύρα</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>UDP port</source>
        <translation>Θύρα UDP</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/RemoteControl/GUI_RemoteControlPreferences.cpp" line="+59"/>
        <source>If activated, Sayonara will answer an UDP request that it is remote controllable</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+36"/>
        <source>Remote control</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+69"/>
        <source>Port %1 already in use</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_SearchPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Search/GUI_SearchPreferences.ui" line="+23"/>
        <source>Example</source>
        <translation>Παράδειγμα</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Case insensitive</source>
        <translation>Δίχως διάκριση πεζών/κεφαλαίων</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Option</source>
        <translation>Επιλογή</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Ignore accents</source>
        <translation>Αγνόηση τονισμού</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Ignore special characters</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_ShortcutPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.ui" line="+44"/>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.cpp" line="+149"/>
        <source>Press shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.cpp" line="-48"/>
        <source>Shortcuts</source>
        <translation>Συντομεύσεις</translation>
    </message>
    <message>
        <location line="+93"/>
        <source>Double shortcuts found</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_StreamRecorderPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/StreamRecorder/GUI_StreamRecorderPreferences.ui" line="+30"/>
        <source>General</source>
        <translation>Γενικά</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Create session directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Automatic recording</source>
        <translation>Αυτόματη εγγραφή</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Target directory</source>
        <translation>Κατάλογος προορισμού</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Session Directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+25"/>
        <source>Choose available placeholders</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+37"/>
        <source>Path template</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+24"/>
        <source>Example</source>
        <translation>Παράδειγμα</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/StreamRecorder/GUI_StreamRecorderPreferences.cpp" line="+173"/>
        <source>Choose target directory</source>
        <translation>Διαλέξτε φάκελο προορισμού</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Target directory is empty</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <location line="+6"/>
        <source>Please choose another directory</source>
        <translation>Παρακαλώ επιλέξτε άλλο φάκελο</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Cannot create %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Template path is not valid</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+59"/>
        <source>Stream recorder</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_StreamPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Streams/GUI_StreamPreferences.ui" line="+17"/>
        <source> ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Buffer size</source>
        <translation>Μέγεθος ενδιάμεσης μνήμης</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show history</source>
        <translation>Εμφάνιση ιστορικού</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Open Streams in new tab</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Streams/GUI_StreamPreferences.cpp" line="+57"/>
        <source>%1 and %2</source>
        <translation>%1 και %2</translation>
    </message>
</context>
<context>
    <name>GUI_CssEditor</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_CssEditor.ui" line="+14"/>
        <location line="+34"/>
        <source>Edit style sheet</source>
        <translation>Επεξεργασία φύλλου αισθητικής επικάλυψης</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Dark mode</source>
        <translation>Σκοτεινή λειτουργία</translation>
    </message>
</context>
<context>
    <name>GUI_FontPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_FontPreferences.ui" line="+84"/>
        <location line="+7"/>
        <location line="+7"/>
        <source>Font size</source>
        <translation>Μέγεθος γραμματοσειράς</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Bold</source>
        <translation>Έντονα</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Font name</source>
        <translation>Όνομα γραμματοσειράς</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_FontPreferences.cpp" line="+129"/>
        <location line="+1"/>
        <source>Inherit</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_IconPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_IconPreferences.ui" line="+20"/>
        <source>Also apply this icon theme to the dark style</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_IconPreferences.cpp" line="+91"/>
        <source>Icons</source>
        <translation>Εικόνες</translation>
    </message>
    <message>
        <location line="+90"/>
        <location line="+63"/>
        <source>System theme</source>
        <translation>Θέμα συστήματος</translation>
    </message>
</context>
<context>
    <name>GUI_UiPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_UiPreferences.ui" line="+57"/>
        <source>Fading cover</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Edit style sheet</source>
        <translation>Επεξεργασία φύλλου αισθητικής επικάλυψης</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_UiPreferences.cpp" line="+48"/>
        <source>User Interface</source>
        <translation>Περιβάλλον χρήστη</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>General</source>
        <translation>Γενικά</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show large cover</source>
        <translation>Εμφάνιση μεγάλου εξώφυλλου</translation>
    </message>
</context>
<context>
    <name>GUI_Shutdown</name>
    <message>
        <location filename="../src/Gui/Shutdown/GUI_Shutdown.ui" line="+14"/>
        <location line="+31"/>
        <source>Shutdown</source>
        <translation>Τερματισμός λειτουργείας</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Shutdown after playlist finished</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+17"/>
        <source>minutes</source>
        <translation>Λεπτά</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Shutdown after</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_SomaFM</name>
    <message>
        <location filename="../src/Gui/SomaFM/GUI_SomaFM.ui" line="+135"/>
        <source>Donate to Soma.fm</source>
        <translation>Δωρεά στο Soma.fm</translation>
    </message>
</context>
<context>
    <name>GUI_SoundcloudArtistSearch</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudArtistSearch.ui" line="+14"/>
        <source>Search Soundcloud</source>
        <translation>Αναζήτηση στο Soundcloud</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Search artist</source>
        <translation>Αναζήτηση καλλιτέχνη</translation>
    </message>
</context>
<context>
    <name>GUI_SoundcloudLibrary</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudLibrary.ui" line="+26"/>
        <source>Library</source>
        <translation>Βιβλιοθήκη</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Search for title, interprets and albums</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_CoverEdit</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.ui" line="+57"/>
        <source>Replace</source>
        <translation>Αντικατάσταση</translation>
    </message>
    <message>
        <location line="+73"/>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.cpp" line="+223"/>
        <source>Original</source>
        <translation>Πρωτότυπο</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.cpp" line="-56"/>
        <location line="+9"/>
        <source>File has no cover</source>
        <translation>Το αρχείο δεν έχει εξώφυλλο</translation>
    </message>
</context>
<context>
    <name>GUI_FailMessageBox</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_FailMessageBox.ui" line="+22"/>
        <source>Details</source>
        <translation>Λεπτομέρειες</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_FailMessageBox.cpp" line="+55"/>
        <source>File exists</source>
        <translation>Το αρχείο υπάρχει</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Writeable</source>
        <translation>Εγγράψιμο</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Some files could not be saved</source>
        <translation>Μερικά αρχεία δεν σώθηκαν</translation>
    </message>
</context>
<context>
    <name>GUI_TagEdit</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagEdit.ui" line="+43"/>
        <source>Discnumber</source>
        <translation>Αριθμός δίσκου</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Album artist</source>
        <translation>Καλλιτέχνης δίσκου</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Comment</source>
        <translation>Σχόλιο</translation>
    </message>
    <message>
        <location line="+150"/>
        <source>all</source>
        <translation>όλα</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Tag from path</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+69"/>
        <source>Read only file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+119"/>
        <source>Undo all</source>
        <translation>Αναίρεση όλων</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagEdit.cpp" line="+160"/>
        <source>Load complete album</source>
        <translation>Φόρτωση ολόκληρου του άλμπουμ</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Metadata</source>
        <translation>Μεταδεδομένα</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tags from path</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+91"/>
        <source>Cannot apply expression to %n track(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ignore these tracks?</source>
        <translation>Παράληψη αυτών των κομματιών;</translation>
    </message>
    <message>
        <location line="+398"/>
        <source>All changes will be lost</source>
        <translation>Όλες οι αλλαγές θα χαθούν</translation>
    </message>
</context>
<context>
    <name>GUI_TagFromPath</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagFromPath.ui" line="+17"/>
        <source>Expression</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+84"/>
        <source>Disc Nr</source>
        <translation>Αριθμός δίσκου</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Apply to all</source>
        <translation>Εφαρμογή σε όλα</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagFromPath.cpp" line="+92"/>
        <source>Tag</source>
        <translation>Καρτέλα</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Please select text first</source>
        <translation>Παρακαλώ πρώτα επιλέξτε κείμενο</translation>
    </message>
</context>
<context>
    <name>StreamServer</name>
    <message>
        <location filename="../src/Components/Broadcasting/StreamServer.cpp" line="+223"/>
        <source>%1 wants to listen to your music.</source>
        <translation>Ο %1 θέλει να ακούσει την μουσική σας</translation>
    </message>
</context>
<context>
    <name>AbstractLibrary</name>
    <message>
        <location filename="../src/Components/Library/AbstractLibrary.cpp" line="+845"/>
        <source>All %1 could be removed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 of %2 %3 could not be removed</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Library::Importer</name>
    <message>
        <location filename="../src/Components/Library/Importer/LibraryImporter.cpp" line="+227"/>
        <source>Cannot import tracks</source>
        <translation>Η εισαγωγή των κομματιών είναι αδύνατη</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>All files could be imported</source>
        <translation>Έγινε η εισαγωγή όλων των αρχείων</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 of %2 files could be imported</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Library::ReloadThread</name>
    <message>
        <location filename="../src/Components/Library/Threads/ReloadThread.cpp" line="+220"/>
        <source>Looking for covers</source>
        <translation>Αναζήτηση εξώφυλλων</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Reading files</source>
        <translation>Ανάγνωση αρχείων</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Deleting orphaned tracks</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Lyrics::LookupThread</name>
    <message>
        <location filename="../src/Components/Lyrics/LyricLookup.cpp" line="+171"/>
        <location line="+35"/>
        <source>Cannot fetch lyrics from %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>No lyrics found</source>
        <translation>Δε βρέθηκαν στίχοι</translation>
    </message>
</context>
<context>
    <name>Shutdown</name>
    <message>
        <location filename="../src/Components/Shutdown/Shutdown.cpp" line="+100"/>
        <source>Computer will shutdown after playlist has finished</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+29"/>
        <location line="+35"/>
        <source>Computer will shutdown in %n minute(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
</context>
<context>
    <name>LastFM::Base</name>
    <message>
        <location filename="../src/Components/Streaming/LastFM/LastFM.cpp" line="+151"/>
        <source>Cannot login to Last.fm</source>
        <translation>Αδύνατη η είσοδος στο Last.fm</translation>
    </message>
</context>
<context>
    <name>SC::JsonParser</name>
    <message>
        <location filename="../src/Components/Streaming/Soundcloud/SoundcloudJsonParser.cpp" line="+123"/>
        <source>Website</source>
        <translation>Ιστότοπος</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+255"/>
        <source>Permalink Url</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-242"/>
        <source>Followers/Following</source>
        <translation>Ακόλουθοι/ακολουθείς</translation>
    </message>
    <message>
        <location line="+92"/>
        <location line="+154"/>
        <source>Purchase Url</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Gui::CoverButton</name>
    <message>
        <location filename="../src/Gui/Covers/CoverButton.cpp" line="+65"/>
        <source>Search an alternative cover</source>
        <translation>Αναζήτηση εναλλακτικού εξώφυλλου</translation>
    </message>
</context>
<context>
    <name>Gui::DoubleCalendarDialog</name>
    <message>
        <location filename="../src/Gui/History/DoubleCalendarDialog.cpp" line="+44"/>
        <source>Select date range</source>
        <translation>Επιλέξτε εύρος ημερομηνίας</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Start date</source>
        <translation>Ημερομηνία έναρξης</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>End date</source>
        <translation>Ημερομηνία λήξης</translation>
    </message>
</context>
<context>
    <name>HistoryContainer</name>
    <message>
        <location filename="../src/Gui/History/HistoryContainer.cpp" line="+25"/>
        <source>History</source>
        <translation>Ιστορικό</translation>
    </message>
</context>
<context>
    <name>GUI_InfoDialog</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_InfoDialog.cpp" line="+105"/>
        <source>Write cover to tracks</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Library::CoverViewContextMenu</name>
    <message>
        <location filename="../src/Gui/Library/CoverView/CoverViewContextMenu.cpp" line="+212"/>
        <source>Toolbar</source>
        <translation>Γραμμή εργαλειών</translation>
    </message>
</context>
<context>
    <name>Library::GUI_CoverView</name>
    <message>
        <location filename="../src/Gui/Library/CoverView/GUI_CoverView.cpp" line="+211"/>
        <source>Use Ctrl + mouse wheel to zoom</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Directory::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/DirectoryContextMenu.cpp" line="+303"/>
        <source>Rename by metadata</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Collapse all</source>
        <translation>Σύμπτυξη όλων</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move to another library</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy to another library</source>
        <translation>Αντιγραφή σε άλλη βιβλιοθήκη</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>View in file manager</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Directory::TreeView</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/DirectoryTreeView.cpp" line="+408"/>
        <source>Copy here</source>
        <translation>Αντιγραφή εδώ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move here</source>
        <translation>Μεταφορά εδώ</translation>
    </message>
</context>
<context>
    <name>GUI_DirectoryView</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/GUI_DirectoryView.cpp" line="+187"/>
        <source>Could not create directory</source>
        <translation>Αδύνατη η δημιουργία φακέλου</translation>
    </message>
</context>
<context>
    <name>Library::GenreView</name>
    <message>
        <location filename="../src/Gui/Library/GenreView.cpp" line="+110"/>
        <source>Updating genres</source>
        <translation>Ενημέρωση ειδών μουσικής</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Do you really want to remove %1 from all tracks?</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Library::GUI_EmptyLibrary</name>
    <message>
        <location filename="../src/Gui/Library/GUI_EmptyLibrary.cpp" line="+94"/>
        <source>Please choose a name for your library</source>
        <translation>Παρακαλώ διαλέξτε ένα όνομα για τη βιβλιοθήκη σας</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Please choose another name for your library</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+24"/>
        <source>The file path is invalid</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>A library with the same file path already exists</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>A library which contains this file path already exists</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Library::HeaderView</name>
    <message>
        <location filename="../src/Gui/Library/Header/HeaderView.cpp" line="+222"/>
        <source>Resize columns</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Resize columns automatically</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Library::TrackModel</name>
    <message>
        <location filename="../src/Gui/Library/TableView/TrackModel.cpp" line="+143"/>
        <source>kBit/s</source>
        <translation>kBit/s</translation>
    </message>
</context>
<context>
    <name>GUI_DeleteDialog</name>
    <message>
        <location filename="../src/Gui/Library/Utils/GUI_DeleteDialog.cpp" line="+88"/>
        <source>Only from library</source>
        <translation>Μόνο από την βιβλιοθήκη</translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>You are about to delete %n file(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
</context>
<context>
    <name>Library::GUI_LibraryReloadDialog</name>
    <message>
        <location filename="../src/Gui/Library/Utils/GUI_ReloadLibraryDialog.cpp" line="+67"/>
        <source>Fast scan</source>
        <translation>Γρήγορη σάρωση</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Deep scan</source>
        <translation>Λεπτομερής σάρωση</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Only scan for new and deleted files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Scan all files in your library directory</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Library::LocalLibraryMenu</name>
    <message>
        <location filename="../src/Gui/Library/Utils/LocalLibraryMenu.cpp" line="+157"/>
        <source>Statistics</source>
        <translation>Στατιστικά</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit library</source>
        <translation>Επεξεργασία βιβλιοθήκης</translation>
    </message>
</context>
<context>
    <name>Gui::MergeMenu</name>
    <message>
        <location filename="../src/Gui/Library/Utils/MergeMenu.cpp" line="+51"/>
        <location line="+59"/>
        <source>Merge</source>
        <translation>Συγχώνευση</translation>
    </message>
</context>
<context>
    <name>GUI_ControlsBase</name>
    <message>
        <location filename="../src/Gui/Player/GUI_ControlsBase.cpp" line="+72"/>
        <source>Sayonara Player</source>
        <translation>Αναπαραγωγέας Sayonara</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Written by %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright</source>
        <translation>Πνευματικά δικαιώματα</translation>
    </message>
</context>
<context>
    <name>Menubar</name>
    <message>
        <location filename="../src/Gui/Player/GUI_PlayerMenubar.cpp" line="+348"/>
        <source>View</source>
        <translation>Προβολή</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+15"/>
        <source>Help</source>
        <translation>Βοήθεια</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>Plugins</source>
        <translation>Πρόσθετα</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Show large cover</source>
        <translation>Εμφάνιση μεγάλου εξώφυλλου</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Fullscreen</source>
        <translation>Πλήρης οθόνη</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Media files</source>
        <translation>Αρχεία πολυμέσων</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open Media files</source>
        <translation>Άνοιγμα αρχείων πολυμέσων</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>For bug reports and feature requests please visit Sayonara&apos;s project page at GitLab</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>FAQ</source>
        <translation>Συχνές ερωτήσεις</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>About Sayonara</source>
        <translation>Σχετικά με το Sayonara</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Written by %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>License</source>
        <translation>Άδεια χρήσης</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Donate</source>
        <translation>Δωρεά</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Thanks to all the brave translators and to everyone who helps building Sayonara packages</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>And special thanks to those people with local music collections</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TrayIconContextMenu</name>
    <message>
        <location filename="../src/Gui/Player/GUI_TrayIcon.cpp" line="+128"/>
        <source>Current song</source>
        <translation>Τρέχον τραγούδι</translation>
    </message>
</context>
<context>
    <name>VersionChecker</name>
    <message>
        <location filename="../src/Gui/Player/VersionChecker.cpp" line="+72"/>
        <source>A new version is available!</source>
        <translation>Νέα έκδοση διαθέσιμη!</translation>
    </message>
</context>
<context>
    <name>GUI_Playlist</name>
    <message>
        <location filename="../src/Gui/Playlist/GUI_Playlist.cpp" line="+252"/>
        <source>Playlist empty</source>
        <translation>Άδεια λίστα τραγουδιών</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Media files</source>
        <translation>Αρχεία πολυμέσων</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open Media files</source>
        <translation>Άνοιγμα αρχείων πολυμέσων</translation>
    </message>
    <message>
        <location line="+206"/>
        <source>Playlist name already exists</source>
        <translation>Το όνομα της λίστας τραγουδιών υπάρχει ήδη</translation>
    </message>
</context>
<context>
    <name>Playlist::ActionMenu</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistActionMenu.cpp" line="+194"/>
        <source>Please set library path first</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Playlist::BottomBar</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistBottomBar.cpp" line="+244"/>
        <source>Please set library path first</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+59"/>
        <source>Cancel shutdown?</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Playlist::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistContextMenu.cpp" line="+171"/>
        <source>Jump to current track</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show track in library</source>
        <translation>Προβολή κομματιού στη βιβλιοθήκη</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Playlist mode</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Playlist::Model</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistModel.cpp" line="+501"/>
        <source>Goto row</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_Crossfader</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Crossfader.cpp" line="+75"/>
        <location line="+12"/>
        <source>Crossfader</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 and %2</source>
        <translation>%1 και %2</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>Crossfader does not work with Alsa</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Gapless playback does not work with Alsa</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_Equalizer</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Equalizer.cpp" line="+119"/>
        <location line="+47"/>
        <source>Linked sliders</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-9"/>
        <source>Equalizer</source>
        <translation>Ισοσταθμιστής (eq)</translation>
    </message>
</context>
<context>
    <name>GUI_LevelPainter</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_LevelPainter.cpp" line="+157"/>
        <source>Level</source>
        <translation>Ένταση</translation>
    </message>
</context>
<context>
    <name>GUI_SpectrogramPainter</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_SpectrogramPainter.cpp" line="+77"/>
        <source>Spectrogram</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_Spectrum</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Spectrum.cpp" line="+150"/>
        <source>Spectrum</source>
        <translation>Φάσμα</translation>
    </message>
</context>
<context>
    <name>GUI_StyleSettings</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_StyleSettings.cpp" line="+203"/>
        <source>There are some unsaved settings&lt;br /&gt;Save now?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+87"/>
        <source>Please specify a name</source>
        <translation>Παρακαλώ προσδιορίστε ένα όνομα</translation>
    </message>
    <message>
        <location line="+168"/>
        <source>Save changes?</source>
        <translation>Να αποθηκευτούν οι αλλαγές;</translation>
    </message>
</context>
<context>
    <name>GUI_PlaylistChooser</name>
    <message>
        <location filename="../src/Gui/Plugins/PlaylistChooser/GUI_PlaylistChooser.cpp" line="+91"/>
        <location line="+38"/>
        <source>No playlists found</source>
        <translation>Δε βρέθηκαν λίστες αναπαραγωγής</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Could not rename playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Name is invalid</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Do you really want to delete %1?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Could not delete playlist %1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Gui::AbstractStationPlugin</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/AbstractStationPlugin.cpp" line="+244"/>
        <source>Cannot open stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+72"/>
        <source>Please choose another name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+36"/>
        <source>Do you really want to delete %1</source>
        <translation>Θέλετε αλήθεια να σβήσετε %1</translation>
    </message>
</context>
<context>
    <name>GUI_Podcasts</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_Podcasts.cpp" line="+65"/>
        <source>Podcast</source>
        <translation>Διαδικτυακή εκπομπή</translation>
    </message>
</context>
<context>
    <name>GUI_Stream</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_Stream.cpp" line="+68"/>
        <source>Search radio station</source>
        <translation>Αναζήτηση ραδιοφωνικού σταθμού</translation>
    </message>
</context>
<context>
    <name>GUI_EnginePreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Engine/GUI_EnginePreferences.cpp" line="+53"/>
        <source>Audio</source>
        <translation>Ήχος</translation>
    </message>
</context>
<context>
    <name>GUI_ShortcutEntry</name>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutEntry.cpp" line="+55"/>
        <source>Enter shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+25"/>
        <source>Shortcut already in use</source>
        <translation>Η συντόμευση χρησιμοποιείται ήδη</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Test</source>
        <translation>Έλεγχος</translation>
    </message>
</context>
<context>
    <name>SomaFM::StationModel</name>
    <message>
        <location filename="../src/Gui/SomaFM/SomaFMStationModel.cpp" line="+125"/>
        <source>Cannot fetch stations</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SC::GUI_ArtistSearch</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudArtistSearch.cpp" line="+84"/>
        <source>Query too short</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+85"/>
        <source>No artists found</source>
        <translation>Δεν βρέθηκαν καλλιτέχνες</translation>
    </message>
    <message numerus="yes">
        <location line="+6"/>
        <source>Found %n artist(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message numerus="yes">
        <location line="+56"/>
        <source>%n playlist(s) found</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
</context>
<context>
    <name>Library::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Utils/ContextMenu/LibraryContextMenu.cpp" line="+196"/>
        <source>Play in new tab</source>
        <translation>Αναπαραγωγή σε νέα καρτέλα</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Standard view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Cover view</source>
        <translation>Προβολή εξώφυλλου</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Directory view</source>
        <translation>Προβολή φακέλου</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Toolbar</source>
        <translation>Γραμμή εργαλειών</translation>
    </message>
    <message>
        <location line="+249"/>
        <source>The toolbar is visible when there are tracks with differing file types listed in the track view</source>
        <translation>Η γραμμή εργαλειών είναι ορατή όταν υπάρχουν κομμάτια διαφορετικών τύπων αρχείου στην προβολή κομματιών</translation>
    </message>
</context>
<context>
    <name>Gui::ImageSelectionDialog</name>
    <message>
        <location filename="../src/Gui/Utils/ImageSelectionDialog.cpp" line="+55"/>
        <source>Image files</source>
        <translation>Αρχεία εικόνας</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Any files</source>
        <translation>Οποιαδήποτε αρχεία</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open image files</source>
        <translation>Άνοιγμα αρχείων εικόνας</translation>
    </message>
</context>
<context>
    <name>Gui::StreamRecorderPreferenceAction</name>
    <message>
        <location filename="../src/Gui/Utils/PreferenceAction.cpp" line="+164"/>
        <location line="+7"/>
        <source>Stream Recorder</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Gui::ShortcutPreferenceAction</name>
    <message>
        <location line="+10"/>
        <location line="+12"/>
        <source>Shortcuts</source>
        <translation>Συντομεύσεις</translation>
    </message>
</context>
<context>
    <name>Gui::MiniSearcher</name>
    <message>
        <location filename="../src/Gui/Utils/SearchableWidget/MiniSearcher.cpp" line="+92"/>
        <source>Arrow up</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Previous search result</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Arrow down</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Next search result</source>
        <translation>Επόμενο αποτέλεσμα αναζήτησης</translation>
    </message>
</context>
<context>
    <name>Gui::LineEdit</name>
    <message>
        <location filename="../src/Gui/Utils/Widgets/LineEdit.cpp" line="+133"/>
        <source>Hint: Use up and down arrow keys for switching between upper and lower case letters</source>
        <translation>Συμβουλή: Χρησιμοποιείστε τα πλήκτρα πάνω και κάτω βελάκι για να εναλλάσετε μεταξύ κεφαλαίων και μικρών γραμμάτων</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove special characters (letters)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Lang</name>
    <message>
        <location filename="../src/Utils/Language/Language.cpp" line="+79"/>
        <source>About</source>
        <translation>Σχετικά</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Action</source>
        <translation>Ενέργεια</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Actions</source>
        <translation>Ενέργειες</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Activate</source>
        <translation>Ενεργοποίηση</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Active</source>
        <translation>Ενεργό</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add</source>
        <translation>Πρόσθεση</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add artist</source>
        <translation>Προσθήκη καλλιτέχνη</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add tab</source>
        <translation>Προσθήκη καρτέλας</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Album</source>
        <translation>Άλμπουμ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Album artist</source>
        <translation>Καλλιτέχνης δίσκου</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Album artists</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Albums</source>
        <translation>Άλμπουμ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>All</source>
        <translation>Όλα</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Append</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Application</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Apply</source>
        <translation>Εφαρμογή</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Artist</source>
        <translation>Καλλιτέχνης</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Artists</source>
        <translation>Καλλιτέχνες</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ascending</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Automatic</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Bitrate</source>
        <translation>Bitrate</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Bookmarks</source>
        <translation>Σελιδοδείκτες</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Broadcast</source>
        <translation>Εκπομπή</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>by</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancel</source>
        <translation>Ακύρωση</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cannot find Lame MP3 encoder</source>
        <translation>Δεν βρέθηκε ο κωδικοποιητής Lame mp3</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Clear</source>
        <translation>Εκκαθάριση</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Clear selection</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Close</source>
        <translation>Κλείσιμο</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close others</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Close tab</source>
        <translation>Κλείσιμο καρτέλας</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Comment</source>
        <translation>Σχόλιο</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Continue</source>
        <translation>Συνέχεια</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Covers</source>
        <translation>Εξώφυλλα</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Created</source>
        <translation>Δημιουργήθηκε</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Create new directory</source>
        <translation>Δημιουργία νέου φακέλου</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Create a new library</source>
        <translation>Δημιουργία νέας βιβλιοθήκης</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dark Mode</source>
        <translation>Σκοτεινή λειτουργεία</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date</source>
        <translation>Ημερομηνία</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Days</source>
        <translation>Ημέρες</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>d</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Default</source>
        <translation>Προκαθορισμένο</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+88"/>
        <source>Delete</source>
        <translation>Διαγραφή</translation>
    </message>
    <message>
        <location line="-86"/>
        <source>Descending</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Directory</source>
        <translation>Φάκελος</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Directories</source>
        <translation>Φάκελοι</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Disc</source>
        <translation>Δίσκος</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Duration</source>
        <translation>Διάρκεια</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Dur.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Dynamic playback</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Edit</source>
        <translation>Επεξεργασία</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Empty input</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Please enter new name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter URL</source>
        <translation>Εισαγάγετε το URL</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Entry</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Entries</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Error</source>
        <translation>Σφάλμα</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fast</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>File</source>
        <translation>Αρχείο</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filename</source>
        <translation>Όνομα αρχείου</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Files</source>
        <translation>Αρχεία</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filesize</source>
        <translation>Μέγεθος αρχείου</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>File type</source>
        <translation>Τύπος αρχείου</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filter</source>
        <translation>Φίλτρο</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>1st</source>
        <translation>1ο</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Font</source>
        <translation>Γραμματοσειρά</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fonts</source>
        <translation>Γραμματοσειρές</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fulltext</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Gapless playback</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Genre</source>
        <translation>Είδος</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Genres</source>
        <translation>Είδη</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hide</source>
        <translation>Απόκρυψη</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hours</source>
        <translation>Ώρες</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>h</source>
        <translation>ω</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Import directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Import files</source>
        <translation>Εισαγωγή αρχείων</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Inactive</source>
        <translation>Ανενεργό</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Info</source>
        <translation>πληροφορίες</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading</source>
        <translation>Φόρτωση</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading %1</source>
        <translation>Φόρτωση %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid characters</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>KB</source>
        <translation>KB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl+f</source>
        <translation>Ctrl+f</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shift</source>
        <translation>Shift</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Backspace</source>
        <translation>Οπισθοδιάστημα</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Tab</source>
        <translation>Καρτέλα</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library</source>
        <translation>Βιβλιοθήκη</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library path</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Library view type</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Listen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Live Search</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Logger</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Log level</source>
        <translation>Επίπεδο καταγραφών</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Lyrics</source>
        <translation>Στίχοι</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>MB</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Menu</source>
        <translation>Μενού</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Minimize</source>
        <translation>Ελαχιστοποίηση</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Minutes</source>
        <translation>Λεπτά</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>m</source>
        <translation>λ´</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Missing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Modified</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Months</source>
        <translation>Μήνες</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mute</source>
        <translation>Σίγαση</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mute off</source>
        <translation>Διακοπή σίγασης</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Name</source>
        <translation>Όνομα</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New</source>
        <translation>Νέο</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Next page</source>
        <translation>Επόμενη σελίδα</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Next track</source>
        <translation>Επόμενο κομμάτι</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>No</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>No albums</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <location line="+144"/>
        <source>Tracks</source>
        <translation>Κομμάτια</translation>
    </message>
    <message>
        <location line="-142"/>
        <source>Move down</source>
        <translation>Μετακίνηση κάτω</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Move up</source>
        <translation>Μετακίνηση πάνω</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>OK</source>
        <translation>Εντάξει</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>on</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Open</source>
        <translation>Άνοιγμα</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open directory</source>
        <translation>Άνοιγμα φακέλου</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open file</source>
        <translation>Άνοιγμα αρχείου</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>or</source>
        <translation>ή</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Overwrite</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Pause</source>
        <translation>Παύση</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play</source>
        <translation>Αναπαραγωγή</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playing time</source>
        <translation>Χρόνος αναπαραγωγής</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play in new tab</source>
        <translation>Αναπαραγωγή σε νέα καρτέλα</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playlist</source>
        <translation>Λίστα τραγουδιών</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playlists</source>
        <translation>Λίστες τραγουδιών</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play next</source>
        <translation>Αναπαραγωγή επόμενου</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play/Pause</source>
        <translation>Αναπαραγωγή/Παύση</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Plugin</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Podcasts</source>
        <translation>Διαδικτυακές εκπομπές</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Preferences</source>
        <translation>Προτιμήσεις</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Previous page</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Previous track</source>
        <translation>Προηγούμενο κομμάτι</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Purchase Url</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Quit</source>
        <translation>Έξοδος</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Radio</source>
        <translation>Ράδιο</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Radio Station</source>
        <translation>Ραδιοφωνικός σταθμός</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Rating</source>
        <translation>Αξιολόγηση</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Really</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Refresh</source>
        <translation>Ανανέωση</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reload Library</source>
        <translation>Επαναφόρτωση της βιβλιοθήκης</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Remove</source>
        <translation>Διαγραφή</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Rename</source>
        <translation>Μετονομασία</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Repeat 1</source>
        <translation>Επανάληψη 1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Repeat all</source>
        <translation>Επανάληψη όλων</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Replace</source>
        <translation>Αντικατάσταση</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reset</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Retry</source>
        <translation>Προσπάθεια ξανά</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reverse order</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Sampler</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Shuffle</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Shutdown</source>
        <translation>Τερματισμός λειτουργείας</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save</source>
        <translation>Αποθήκευση</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save as</source>
        <translation>Αποθήκευση ως</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save to file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Scan for audio files</source>
        <translation>Σάρωση για αρχεία ήχου</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+2"/>
        <source>Search</source>
        <translation>Αναζήτηση</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search next</source>
        <translation>Αναζήτηση επόμενου</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search previous</source>
        <translation>Αναζήτηση προηγούμενου</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>2nd</source>
        <translation>2ο</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seconds</source>
        <translation>Δευτερόλεπτα</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>s</source>
        <translation>δ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seek backward</source>
        <translation>Αναζήτηση προς τα πίσω</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seek forward</source>
        <translation>Αναζήτηση προς τα εμπρός</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show</source>
        <translation>Εμφάνιση</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Album Artists</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Covers</source>
        <translation>Εμφάνιση εξώφυλλων</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Library</source>
        <translation>Εμφάνιση βιβλιοθήκης</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Similar artists</source>
        <translation>Παρόμοιοι καλλιτέχνες</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sort by</source>
        <translation>Ταξινόμηση κατά</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stop</source>
        <translation>Στοπ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Streams</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Stream URL</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Success</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>th</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>3rd</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Title</source>
        <translation>Τίτλος</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Track</source>
        <translation>Κομμάτι</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Track number</source>
        <translation>Αριθμός κομματιού</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>track on</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Tree</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Undo</source>
        <translation>Αναίρεση</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown album</source>
        <translation>Άγνωστο άλμπουμ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown artist</source>
        <translation>Άγνωστος καλλιτέχνης</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown title</source>
        <translation>Άγνωστος τίτλος</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown genre</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown placeholder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown year</source>
        <translation>Άγνωστο έτος</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Various</source>
        <translation>Διάφορα</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various albums</source>
        <translation>Διάφορα άλμπουμ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various artists</source>
        <translation>Διάφοροι καλλιτέχνες</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various tracks</source>
        <translation>Διάφορα κομμάτια</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Version</source>
        <translation>Έκδοση</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Volume down</source>
        <translation>Μείωση έντασης</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Volume up</source>
        <translation>Αύξηση έντασης</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Warning</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Weeks</source>
        <translation>Εβδομάδες</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Year</source>
        <translation>Έτος</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Years</source>
        <translation>Έτη</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>Ναι</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Zoom</source>
        <translation>Εστίαση</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>No directories</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n directory(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No files</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n file(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No playlists</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n playlist(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No tracks</source>
        <translation>Κανένα κομμάτι</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n track(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No tracks found</source>
        <translation>Δε βρέθηκαν κομμάτια</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n track(s) found</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
</context>
</TS>